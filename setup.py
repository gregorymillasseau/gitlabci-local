#!/usr/bin/env python3

# Standard libraries
from typing import List

# Modules libraries
from setuptools import find_packages, setup

# Requirements
requirements: List[str] = []
with open('requirements/runtime.txt', encoding='utf8', mode='r') as f:
    requirements = [line for line in f.read().splitlines() if not line.startswith('#')]

# Long description
long_description: str = '' # pylint: disable=invalid-name
with open('README.md', encoding='utf8', mode='r') as f:
    long_description = f.read()

# Project configurations
PROJECT_AUTHOR = 'Adrian DC'
PROJECT_DESCRIPTION = 'Launch .gitlab-ci.yml jobs locally'
PROJECT_EMAIL = 'radian.dc@gmail.com'
PROJECT_KEYWORDS = 'gitlab-ci local gcil pipeline'
PROJECT_NAME = 'gitlabci-local'
PROJECT_OWNER = 'AdrianDC'
PROJECT_PACKAGE = 'gitlabci-local'
PROJECT_SCRIPTS = [
    'gitlabci-local = gitlabci_local.cli.main:main',
    'gcil = gitlabci_local.cli.main:main',
]

# Setup configurations
setup(
    name=PROJECT_PACKAGE,
    use_scm_version=True,
    author=PROJECT_AUTHOR,
    author_email=PROJECT_EMAIL,
    license='Apache License 2.0',
    description=PROJECT_DESCRIPTION,
    long_description=long_description,
    long_description_content_type='text/markdown',
    url=f'https://gitlab.com/{PROJECT_OWNER}/{PROJECT_NAME}',
    project_urls={
        'Bug Reports': f'https://gitlab.com/{PROJECT_OWNER}/{PROJECT_NAME}/-/issues',
        'Changelog': f'https://gitlab.com/{PROJECT_OWNER}/{PROJECT_NAME}/blob/master/CHANGELOG.md',
        'Documentation': f'https://gitlab.com/{PROJECT_OWNER}/{PROJECT_NAME}#{PROJECT_PACKAGE}',
        'Source': f'https://gitlab.com/{PROJECT_OWNER}/{PROJECT_NAME}',
        'Statistics': 'https://pypistats.org/packages/{PROJECT_PACKAGE}'
    },
    packages=find_packages(exclude=['tests']),
    setup_requires=['setuptools_scm'],
    install_requires=requirements,
    classifiers=[
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
        'Topic :: Software Development',
        'Topic :: Utilities',
    ],
    keywords=PROJECT_KEYWORDS,
    python_requires='>=3, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*, !=3.5.*',
    entry_points={
        'console_scripts': PROJECT_SCRIPTS,
    },
)
